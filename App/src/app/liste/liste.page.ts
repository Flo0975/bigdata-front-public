import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste',
  templateUrl: './liste.page.html',
  styleUrls: ['./liste.page.scss'],
})
export class ListePage implements OnInit {
  public r1 = [];

  // tslint:disable-next-line: variable-name
  constructor(private dataService: DataService, private router: Router) {
   }

  ngOnInit() {
    this.dataService.getRemoteData().subscribe(data => this.r1 = data);
  }

  public openSpectacle = (id: string): void => {
    this.router.navigateByUrl(`/info-spectacle/${id}`);
  }

}
