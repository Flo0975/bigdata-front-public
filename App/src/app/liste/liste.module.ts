import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';

import { ListePageRoutingModule } from './liste-routing.module';

import { ListePage } from './liste.page';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ListePageRoutingModule
  ],
  declarations: [ListePage]
})
export class ListePageModule {}
