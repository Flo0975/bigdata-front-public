export class Form {
    budget: number;
    bus: boolean;
    velo: boolean;
    voiture: boolean;
    theatre: boolean;
    musique: boolean;
    danse: boolean;
    lecture: boolean;
    ateliers: boolean;
    cinema: boolean;
    meeting: boolean;
    congres: boolean;
    contes: boolean;
    adultes: number;
    enfants: number;

}
