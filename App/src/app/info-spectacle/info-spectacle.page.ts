import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { ListesSpect } from '../interfaces/listes.interfaces';
import { ModalController } from '@ionic/angular';
import { MapComponent } from './map/map.component';

@Component({
  selector: 'app-info-spectacle',
  templateUrl: './info-spectacle.page.html',
  styleUrls: ['./info-spectacle.page.scss'],
})
export class InfoSpectaclePage implements OnInit {
  public list: ListesSpect = undefined;
  public isReady = false;
  constructor(private activatedRoute: ActivatedRoute, 
              private dataService: DataService, 
              private router: Router, 
              private modalController: ModalController) { }

  ngOnInit() {
    this.initialization().then(value => {
      this.isReady = value;
    });
  }
  private initialization = async (): Promise<boolean> => {
    this.list = await this.fetchList();
    console.log(this.list);
    return true;
  }

  public openModal = async (id: number): Promise<void> => {
    const modal = await this.modalController.create(
      {
        component: MapComponent,
        componentProps: {
          'id': id
        }
      }
    )
    return await modal.present();
  }

  private fetchList = (): Promise<ListesSpect> => {
    return new Promise((resolve, reject) => {
      this.activatedRoute.params.subscribe(params => {
        this.dataService.getListData(params.id).subscribe(response => {
          resolve(response.body);
        });
      });
    });
  }


}
