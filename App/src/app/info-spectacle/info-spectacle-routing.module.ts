import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoSpectaclePage } from './info-spectacle.page';

const routes: Routes = [
  {
    path: '',
    component: InfoSpectaclePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoSpectaclePageRoutingModule {}
