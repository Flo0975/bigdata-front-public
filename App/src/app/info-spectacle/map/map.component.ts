import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { ListesSpect } from 'src/app/interfaces/listes.interfaces';
import { Marker } from 'src/app/interfaces/marker.interfaces';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {

  // tslint:disable-next-line: max-line-length
  constructor(private modalController: ModalController, private dataService: DataService, private activatedRoute: ActivatedRoute, private geolocation: Geolocation) { }
  public zoom = 15;
  public lat = 43.9437782;
  public lng = 4.8048022;
  public isReady = false;
  public dir = undefined;
  public origin: any;
  public destination: any;

  public marker: Marker;
  public nameList: ListesSpect;
  @Input() id: string;

  ionViewDidEnter() {
    this.getMarkers();
  }
  ngOnInit() {}

  public onClose = async (): Promise<void> => {
    this.modalController.dismiss();
  }

  private getMarkers = (): Promise<void> => {
    return new Promise((resolve, reject) => {
        this.dataService.getListData(this.id).subscribe(response => {
          console.log(response.body);
          this.marker = {
            coordonnes: response.body.coordonnees
          };
          this.nameList = {
            nom: response.body.nom,
            date: response.body.date,
            resume: response.body.resume,
            lieu: response.body.lieu,
            types: response.body.types,
            coordonnees: response.body.coordonnees,
          };
          // console.log(this.marker.coordonnes);
          const y = response.body.coordonnees;
          console.log(y);
          this.isReady = true;
        });
      });
  }
  getDirection(lat, lng) {
    this.dir = {
      origin: { lat, lng },
      destination: { lat: this.marker.coordonnes.lng, lng: this.marker.coordonnes.lat }
    }
  }
  locate(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.getDirection(resp.coords.latitude, resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }

}
