import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfoSpectaclePage } from './info-spectacle.page';

describe('InfoSpectaclePage', () => {
  let component: InfoSpectaclePage;
  let fixture: ComponentFixture<InfoSpectaclePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoSpectaclePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfoSpectaclePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
