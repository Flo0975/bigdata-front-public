import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';

import { IonicModule } from '@ionic/angular';

import { InfoSpectaclePageRoutingModule } from './info-spectacle-routing.module';

import { InfoSpectaclePage } from './info-spectacle.page';
import { MapComponent } from './map/map.component';
import { StationnerPageModule } from '../stationner/stationner.module';
import { AgmDirectionModule } from 'agm-direction';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfoSpectaclePageRoutingModule,
    AgmCoreModule.forRoot({apiKey: 'API'}),
    AgmDirectionModule
  ],
  declarations: [InfoSpectaclePage, MapComponent],
  entryComponents: [MapComponent]
})
export class InfoSpectaclePageModule {}
