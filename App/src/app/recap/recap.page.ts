import { Component, OnInit } from '@angular/core';
import { SendformService } from '../services/sendform.service';
import { FormService } from '../services/form.service';
//import { Geolocation } from '@ionic-native/geolocation/ngx';

import { Router } from '@angular/router';

@Component({
  selector: 'app-recap',
  templateUrl: './recap.page.html',
  styleUrls: ['./recap.page.scss'],
})
export class RecapPage implements OnInit {

  constructor(private formService: FormService, 
    private router: Router, 
    private sendFormService: SendformService) { }
    public recapBudget = this.formService.data.budget;
    public adultes = this.formService.data.adultes;
    public enfants = this.formService.data.enfants;

  private test = this.formService.data.velo;
  public themesFavoris = [
    this.formService.data.lecture,
    this.formService.data.theatre,
    this.formService.data.cinema,
    this.formService.data.cinemajeune,
    this.formService.data.danse,
    this.formService.data.atelierP,
    this.formService.data.musique,
    this.formService.data.conference,
    this.formService.data.feuilleton,
    this.formService.data.indiscipline,
    this.formService.data.theatreJ,
    this.formService.data.visites
  ];

  public transport = [
    this.formService.data.bus,
    this.formService.data.velo,
    this.formService.data.voiture
  ];

  ngOnInit() {

  }

  async onSubmit() {
    await this.sendFormService.Send();
    this.router.navigateByUrl('/info-spec-perso');
  }
}
