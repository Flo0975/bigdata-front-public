import { Coordonnees } from './coordonnes.interfaces';

export interface Marker {
    name?: string;
    coordonnes: Coordonnees;
}