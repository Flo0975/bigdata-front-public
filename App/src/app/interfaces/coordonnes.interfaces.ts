export interface Coordonnees {
    lat: number;
    lng: number;
}