import { Coordonnees } from './coordonnes.interfaces';

export interface Parking {
    id?: number;
    nom: string;
    coordonnees?: Coordonnees;
}