import { Coordonnees } from './coordonnes.interfaces';

export interface ListesSpect {
    id?: number;
    price?: number;
    nom: string;
    date: string;
    startDate?: string;
    lat?: number;
    lng?: number;
    places_reserved?: number;
    resume: string;
    types: string;
    lieu: string;
    affluence?: number;
    places_max?: number;
    coordonnees: Coordonnees;
}
