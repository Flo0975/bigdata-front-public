import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoSpecPersoPage } from './info-spec-perso.page';

const routes: Routes = [
  {
    path: '',
    component: InfoSpecPersoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoSpecPersoPageRoutingModule {}
