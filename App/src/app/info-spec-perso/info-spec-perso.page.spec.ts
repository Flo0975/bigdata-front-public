import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfoSpecPersoPage } from './info-spec-perso.page';

describe('InfoSpecPersoPage', () => {
  let component: InfoSpecPersoPage;
  let fixture: ComponentFixture<InfoSpecPersoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoSpecPersoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfoSpecPersoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
