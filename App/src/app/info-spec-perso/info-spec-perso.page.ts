import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { SendformService } from '../services/sendform.service';

@Component({
  selector: 'app-info-spec-perso',
  templateUrl: './info-spec-perso.page.html',
  styleUrls: ['./info-spec-perso.page.scss'],
})
export class InfoSpecPersoPage implements OnInit {
  public r1 = [];
  constructor(private sendForm: SendformService, private router: Router) { }

  ngOnInit() {
    this.sendForm.Send().subscribe(data => this.r1 = data);
  }

  public openSpectacle = (id: string): void => {
    this.router.navigateByUrl(`/info-spectacle/${id}`);
  }
}
