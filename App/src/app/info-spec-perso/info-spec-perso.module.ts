import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoSpecPersoPageRoutingModule } from './info-spec-perso-routing.module';

import { InfoSpecPersoPage } from './info-spec-perso.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfoSpecPersoPageRoutingModule
  ],
  declarations: [InfoSpecPersoPage]
})
export class InfoSpecPersoPageModule {}
