import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  public zoom = 15;
  public lat = 43.9437782;
  public lng = 4.8048022;
  public isReady = true;
  public dir = undefined;
  public origin: any;
  public destination: any;
  constructor(private dataService: DataService, private geolocation: Geolocation) { }

  ngOnInit() {
    // this.getDirection();
  }

  getDirection(lat, lng) {
    this.dir = { 
      origin: { lat, lng },
      destination: { lat: 43.95157522839205, lng: 4.807267148656406 }
    }
  }
  locate(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.getDirection(resp.coords.latitude, resp.coords.longitude);
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
}
