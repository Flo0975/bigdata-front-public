import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StationnerPage } from './stationner.page';

const routes: Routes = [
  {
    path: '',
    component: StationnerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StationnerPageRoutingModule {}
