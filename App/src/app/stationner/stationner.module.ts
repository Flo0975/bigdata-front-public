import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StationnerPageRoutingModule } from './stationner-routing.module';

import { StationnerPage } from './stationner.page';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    IonicModule,
    StationnerPageRoutingModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyDraPfQXFZc7z_fls8AeZG2kOZfuLg8etM'})
  ],
  declarations: [StationnerPage]
})
export class StationnerPageModule {}
