import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StationnerPage } from './stationner.page';

describe('StationnerPage', () => {
  let component: StationnerPage;
  let fixture: ComponentFixture<StationnerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationnerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StationnerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
