import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Marker } from '../interfaces/marker.interfaces';
import { Parking } from '../interfaces/parking.interfaces';

@Component({
  selector: 'app-stationner',
  templateUrl: './stationner.page.html',
  styleUrls: ['./stationner.page.scss'],
})
export class StationnerPage implements OnInit {

  constructor(private dataService: DataService) { }

  public zoom = 15;
  public lat = 43.9437782;
  public lng = 4.8048022;
  public isReady: boolean = false;
  public markers: Marker[] = [];

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.getMarkers();
  }
  private getMarkers = (): void => {
    this.dataService.getListParking().subscribe(result => {
      for (var i = 0; i in result.body ; i++) {
        this.markers.push({
          coordonnes: {
                      lat: result.body[i].coordonnees.lat,
                      lng: result.body[i].coordonnees.lng
                    },
          name: result.body[i].nom
          });
      }
      this.isReady = true;
    });
  }

}
