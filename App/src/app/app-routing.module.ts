import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'launch', pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'launch',
    loadChildren: () => import('./launch/launch.module').then( m => m.LaunchPageModule)
  },
  {
    path: 'form',
    loadChildren: () => import('./form/form.module').then( m => m.FormPageModule)
  },
  {
    path: 'liste',
    loadChildren: () => import('./liste/liste.module').then( m => m.ListePageModule)
  },
  {
    path: 'stationner',
    loadChildren: () => import('./stationner/stationner.module').then( m => m.StationnerPageModule)
  },
  {
    path: 'map',
    loadChildren: () => import('./map/map.module').then( m => m.MapPageModule)
  },
  {
    path: 'recap',
    loadChildren: () => import('./recap/recap.module').then( m => m.RecapPageModule)
  },
  {
    path: 'info-spectacle/:id',
    loadChildren: () => import('./info-spectacle/info-spectacle.module').then( m => m.InfoSpectaclePageModule)
  },
  {
    path: 'info-spec-perso',
    loadChildren: () => import('./info-spec-perso/info-spec-perso.module').then( m => m.InfoSpecPersoPageModule)
  }



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
