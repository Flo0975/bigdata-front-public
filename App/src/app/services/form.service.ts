import { Injectable } from '@angular/core';
import { Coordonnees } from '../interfaces/coordonnes.interfaces';

export interface Data {
  budget: number;
  adultes: number;
  enfants: number;
  velo: string;
  bus: string;
  voiture: string;
  atelierP: string;
  cinema: string;
  cinemajeune: string;
  conference: string;
  danse: string;
  feuilleton: string;
  indiscipline: string;
  lecture: string;
  musique: string;
  theatre: string;
  theatreJ: string;
  visites: string;
}
@Injectable({
  providedIn: 'root'
})
export class FormService {
  public data: Data = undefined;

  constructor() { }

  public add(data: Data) {
    this.data = data;

    console.log('nous sommes dans le serviceform' + this.data.atelierP  + this.data.danse);
  }
}
