import { Injectable } from '@angular/core';
import { Data, Router } from '@angular/router';
import { FormService } from '../services/form.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ListesSpect } from '../interfaces/listes.interfaces';

@Injectable({
  providedIn: 'root'
})
export class SendformService {
  constructor(private formService: FormService, private http: HttpClient, private router: Router) { }
  private path = 'http://10.31.1.174:4567/returnFullEvents?budget=100&types=Cin%C3%A9ma,Visites&startDate=';


  public Send(): Observable<ListesSpect[]> {

    var budget = this.formService.data.budget;
    var types = [
                this.formService.data.cinema,
                this.formService.data.cinemajeune,
                this.formService.data.conference,
                this.formService.data.atelierP,
                this.formService.data.danse,
                this.formService.data.feuilleton,
                this.formService.data.indiscipline,
                this.formService.data.lecture,
                this.formService.data.musique,
                this.formService.data.theatre,
                this.formService.data.theatreJ,
                this.formService.data.visites
    ]

    var PATTERN = 'null',
    types = types.filter(function (str) { return str.indexOf(PATTERN) === -1; });

    var transport = [
                this.formService.data.velo,
                this.formService.data.bus,
                this.formService.data.voiture
    ]
    var PATTERN2 = 'null',
    transport = transport.filter(function (str) { return str.indexOf(PATTERN2) === -1; });

    const adultes = this.formService.data.adultes;
    const enfants = this.formService.data.enfants;

    const url = 'http://10.31.1.174:4567/returnFullEvents?' + 'budget=' + budget + '&' + 'types=' + types.join(','); //PROD
    console.log(url);

    return this.http.get<ListesSpect[]>(url);
  }


}
