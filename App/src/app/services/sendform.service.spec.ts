import { TestBed } from '@angular/core/testing';

import { SendformService } from './sendform.service';

describe('SendformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SendformService = TestBed.get(SendformService);
    expect(service).toBeTruthy();
  });
});
