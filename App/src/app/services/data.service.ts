import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ListesSpect } from '../interfaces/listes.interfaces';
import { Observable } from 'rxjs';
import { Parking } from '../interfaces/parking.interfaces';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

   // private path = 'http://10.31.1.174:4567'; //PROD
   private path = 'http://localhost:9999'; //Local

  getRemoteData(): Observable<ListesSpect[]> {
    // const url: string = `${this.path}/returnFullEvents`;  // PROD
     const url: string = `${this.path}/liste`;  // Local
     return this.http.get<ListesSpect[]>(url);
  }

  getListDataPerso(): Observable<ListesSpect[]> {
    const url: string = `${this.path}/perso`; // local
    return this.http.get<ListesSpect[]>(url);
  }

  getListParking2(): Observable<Parking[]> {
    // const url: string = `${this.path}/getAllParking`;  // Prod
     const url: string = `${this.path}/parking`;  // Local
    return this.http.get<Parking[]>(url);
  }


  getListParking(): Observable<HttpResponse<Parking>>{
    // Set URL
    // const url: string = `${this.path}/getAllParking`; // Prod
    const url: string = `${this.path}/parking`; // local
    // Set Request Options
    const OPTIONS = {
      headers: new HttpHeaders({
        Accept: "application/json"
      }),
      observe: "response" as "body"
    };
    return this.http.get<HttpResponse<Parking>>(url, OPTIONS);
  }

  public getListData = (id: string): Observable<HttpResponse<ListesSpect>> => {
    // Set URL
    // const url: string = `${this.path}/getEvent?id=${id}`; // Prod
    const url: string = `${this.path}/infos-spec/${id}`; // Local
    // Set Request Options
    const OPTIONS = {
      headers: new HttpHeaders({
        Accept: "application/json"
      }),
      observe: "response" as "body"
    };

    return this.http.get<HttpResponse<ListesSpect>>(url, OPTIONS);
  }

}
