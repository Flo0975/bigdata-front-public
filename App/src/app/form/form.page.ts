import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FormService } from '../services/form.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit {
  public adultes = 0;
  public enfants = 0;
  public budget = 20;
  public velo = false;
  public bus = false; 
  public voiture = false;

  public atelierP = false;
  public cinema = false;
  public cinemajeune = false;
  public conference = false;
  public danse = false;
  public feuilleton = false;
  public indiscipline = false;
  public lecture = false;
  public musique = false;
  public theatre = false;
  public theatreJ = false;
  public visites = false;

  public veloText = '';
  public busText = '';
  public voitureText = '';

  public atelierPText = '';
  public cinemaText = '';
  public cinemajeuneText = '';
  public conferenceText = '';
  public danseText = '';
  public feuilletonText = '';
  public indisciplineText = '';
  public lectureText = '';
  public musiqueText = '';
  public theatreText = '';
  public theatreJText = '';
  public visitesText = '';
  public data = '';


  public form: FormGroup = new FormGroup({
    budget: new FormControl(''),
    velo: new FormControl(''),
    bus: new FormControl(''),
    voiture: new FormControl(''),
    adultes: new FormControl(''),
    enfants: new FormControl(''),
    atelierP: new FormControl(''),
    cinema: new FormControl(''),
    cinemajeune: new FormControl(''),
    conference: new FormControl(''),
    danse: new FormControl(''),
    feuilleton: new FormControl(''),
    indiscipline: new FormControl(''),
    lecture: new FormControl(''),
    musique: new FormControl(''),
    theatre: new FormControl(''),
    theatreJ: new FormControl(''),
    visites: new FormControl('')
  });
  constructor(private formService: FormService, private router: Router) {
  }

  ngOnInit() {
  }

  async onSubmit() {
    if (this.velo === true){
      this.veloText = 'vélo';
    } else {
      this.veloText = 'null';
    }

    if (this.bus === true){
      this.busText = 'bus/tram';
    } else {
      this.busText = 'null';
    }

    if (this.voiture === true){
      this.voitureText = 'voiture';
    } else {
      this.voitureText = 'null';
    }
    if (this.theatre === true){
      this.theatreText = 'Théâtre';
    } else {
      this.theatreText = 'null';
    }

    if (this.musique === true){
      this.musiqueText = 'Musique';
    } else {
      this.musiqueText = 'null';
    }

    if (this.danse === true){
      this.danseText = 'Danse';
    } else {
      this.danseText = 'null';
    }

    if (this.lecture === true){
      this.lectureText = 'Lecture';
    } else {
      this.lectureText = 'null';
    }

    if (this.atelierP === true){
      this.atelierPText = 'Atelier de la pensée';
    } else {
      this.atelierPText = 'null';
    }

    if (this.cinema === true){
      this.cinemaText = 'Cinéma';
    } else {
      this.cinemaText = 'null';
    }

    if (this.cinemajeune === true){
      this.cinemajeuneText = 'Cinéma Jeune';
    } else {
      this.cinemajeuneText = 'null';
    }

    if (this.conference === true){
      this.conferenceText = 'Conférences';
    } else {
      this.conferenceText = 'null';
    }

    if (this.feuilleton === true){
      this.feuilletonText = 'Feuilleton';
    } else {
      this.feuilletonText = 'null';
    }
    if (this.indiscipline === true){
      this.indisciplineText = 'Indiscipline';
    } else {
      this.indisciplineText = 'null';
    }
    if (this.lecture === true){
      this.lectureText = 'Lecture';
    } else {
      this.lectureText = 'null';
    }
    if (this.theatreJ === true){
      this.theatreJText = 'Théâtre';
    } else {
      this.theatreJText = 'null';
    }

    if (this.visites === true){
      this.visitesText = 'Visites';
    } else {
      this.visitesText = 'null';
    }
//ici
    await this.formService.add(
      {
        budget: this.budget,
        adultes : this.adultes,
        enfants : this.enfants,
        velo : this.veloText,
        bus : this.busText,
        voiture: this.voitureText,
        atelierP: this.atelierPText,
        cinema: this.cinemaText,
        cinemajeune: this.cinemajeuneText,
        conference: this.conferenceText,
        danse: this.danseText,
        feuilleton: this.feuilletonText,
        indiscipline: this.indisciplineText,
        lecture: this.lectureText,
        musique: this.musiqueText,
        theatre: this.theatreText,
        theatreJ: this.theatreJText,
        visites: this.visitesText
      }
      );
    if (this.budget == 10) {
      console.log('redirection');
      this.router.navigateByUrl('/recap');
    } else {
      this.router.navigateByUrl('/recap');
    }
  }
}
